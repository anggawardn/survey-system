import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const getSurveyUrl = "api/survey/find-all-survey";
const addSurveyUrl = "api/survey/add-all-at-once";
const headerConfig = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
};

@Injectable({
    providedIn: 'root'
})
export class SurveyQuestionService {
    
    constructor(private http:HttpClient) {}

    getCards(): Observable<any[]> {
        return this.http.get<any[]>(getSurveyUrl, headerConfig); 
    }

    create(form): Observable<any>{ 
        return this.http.post<any>(addSurveyUrl, form, headerConfig);
    }

}