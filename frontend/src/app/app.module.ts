import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthentificationModule } from './auth';
import { AppMaterialModule } from './shared/app-material.module';

import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';

import { CreateComponent } from './survey/create/create.component';
import { ShowComponent } from './survey/show/show.component';
import { NavbarComponent } from './component/navbar/navbar.component';
import { CardsComponent } from './component/cards/cards.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';

@NgModule({
  declarations: [
    AppComponent,
    CreateComponent,
    ShowComponent,
    NavbarComponent,
    CardsComponent,
    LoginComponent,
    RegisterComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    AuthentificationModule,
    AppMaterialModule,
    ReactiveFormsModule,
    FlexLayoutModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
