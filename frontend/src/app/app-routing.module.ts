import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateComponent } from './survey/create/create.component';

import { PublicGuard, ProtectedGuard } from 'ngx-auth';

import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { CardsComponent } from './component/cards/cards.component';


const routes: Routes = [
  {path: 'login', component: LoginComponent, canActivate: [PublicGuard]},
  {path: 'register', component: RegisterComponent, canActivate: [PublicGuard]},
  {path: 'cards', component: CardsComponent, canActivate: [ProtectedGuard]},
  {path: 'create', component: CreateComponent, canActivate: [ProtectedGuard]},
  {path: '', component: CardsComponent, canActivate: [ProtectedGuard]},
  {path: '**', redirectTo: '', canActivate: [ProtectedGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }