import { Component, OnInit, ViewChildren } from '@angular/core';
import { FormGroup, Validators, FormArray, FormControl, NgForm } from '@angular/forms';
import { CdkDragDrop, moveItemInArray } from "@angular/cdk/drag-drop";
import { SurveyQuestionService } from 'src/app/shared/survey/survey.question.service';
import { MatTableDataSource, MatTable } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  formGroup: FormGroup;
  title = 'Untitled';
  url = '';
  displayedColumns: string[] = ['no', 'ket', 'img', 'del'];
  option = [
    { icon: 'sort', teks: 'Short Answer', nilai: 'SHORT_ANSWER' },
    { icon: 'subject', teks: 'Long Answer', nilai: 'LONG_ANSWER' },
    { icon: 'radio_button_checked', teks: 'Multiple Choice', nilai: 'MULTIPLE' },
    { icon: 'check_box', teks: 'Checkboxes', nilai: 'CHECKBOX' },
    { icon: 'arrow_drop_down_circle', teks: 'Dropdown', nilai: 'DROPDOWN' },
    { icon: 'cloud_upload', teks: 'File Upload', nilai: 'FILE_UPLOAD' },
    { icon: 'linear_scale', teks: 'Linear Scale', nilai: 'LINEAR_SCALE' },
    { icon: 'more_vert', teks: 'Multiple choice grid', nilai: 'MULTI_CHOICE_MULTI'},
    { icon: 'apps', teks: 'Checkbox grid', nilai: 'CHECKBOX_GRID' },
    { icon: 'insert_invitation', teks: 'Date', nilai: 'DATE_TYPE' },
    { icon: 'access_time', teks: 'Time', nilai: 'TIME_TYPE' }
  ]

  constructor(private serviceQ: SurveyQuestionService, private _router: Router){}

  ngOnInit(){
    this.section();
  }

  /* Build Form Group Start */
  get form() { return this.formGroup.controls; }

  public section(): void{
    this.formGroup = new FormGroup({
      'title': new FormControl('Survey'),
      'desc': new FormControl(''),
      'pages': new FormArray([
        this.initpages()
      ])
    });
  }

  public initpages(): FormGroup{
    return new FormGroup({
      'title': new FormControl(''),
      'desc': new FormControl(''),
      'questions': new FormArray([
        this.initquestions()
      ])
    });
  }
  
  public initquestions(): FormGroup{
    return new FormGroup({
      'question': new FormControl(''),
      'questionType': new FormControl(''),
      optionAnswer: new FormArray([
        this.initoptionAnswer()
      ])
    });
  }

  public initoptionAnswer(): FormGroup{
    return new FormGroup({
      'value': new FormControl('')
    });
  }

  /* Build Form Group End */

  public addpages(): void{
    const control = <FormArray>this.form.pages;
    control.push(this.initpages());
  }

  public removepages(i){
    const control = <FormArray>this.form.pages;
    control.removeAt(i);
  }

  public addquestions(i): void{
    const control = (<FormArray>this.form.pages).at(i).get('questions') as FormArray;
    control.push(this.initquestions());
  }

  public removequestions(i){
    const control = (<FormArray>this.form.pages).at(i).get('questions') as FormArray;
    control.removeAt(i);
  }

  public addOption(i,j){
    const control = ((<FormArray>this.form.pages).at(i).get('questions') as FormArray).at(j).get('optionAnswer') as FormArray;
    control.push(this.initoptionAnswer());
  }

  public removeOption(i,j){
    const control = ((<FormArray>this.form.pages).at(i).get('questions') as FormArray).at(j).get('optionAnswer') as FormArray;
    control.removeAt(i);
  }
  
  onSubmit(formData: NgForm){
    if(this.formGroup.valid){
      this.serviceQ.create(formData).subscribe(
        res => console.log('create : ', res)
      );
      alert('Summit success!');
      this._router.navigate(['dashboard']);
    }
  }

  addImage(event){
    console.log('image')
  }

}
