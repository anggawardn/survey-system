import { Component, OnInit } from '@angular/core';
import { SurveyQuestionService } from 'src/app/shared/survey/survey.question.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css']
})
export class CardsComponent implements OnInit {
  cards: any[];
  dataSource: any;
  Survey: any;

  constructor(private surveyService: SurveyQuestionService, private router: Router) { }

  ngOnInit() {
    this.surveyService.getCards().subscribe( cards => this.cards = cards);
  }

  coba () {
    console.log("coba");
  }

  create () {
    this.router.navigateByUrl('/create');
  }

}
