import { Component, OnInit, Input } from '@angular/core';
import { AuthentificationService } from 'src/app/auth';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  @Input() title: string;
  constructor(private authService: AuthentificationService) {}

  ngOnInit() {
  }

  logout() {
    return this.authService.logout();
  }
}
