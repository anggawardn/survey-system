#!/bin/sh

curl -i -X POST --url http://localhost:8001/services/ --data 'name=frontend' --data 'url=http://172.17.0.1:4200'

curl -i -X POST --url http://localhost:8001/services/frontend/routes --data 'paths=/'

curl -i -X POST --url http://localhost:8001/services/ --data 'name=auth' --data 'url=http://172.17.0.1:1313'

curl -i -X POST --url http://localhost:8001/services/auth/routes --data 'paths=/api/auth'

curl -i -X POST --url http://localhost:8001/services/ --data 'name=survey' --data 'url=http://172.17.0.1:9090'

curl -i -X POST --url http://localhost:8001/services/survey/routes  --data 'paths=/api/survey'

curl -X POST http://localhost:8001/services/survey/plugins --data "name=jwt"

curl -X POST http://localhost:8001/consumers --data "username=curl"
