#!/bin/sh

curl -X POST http://localhost:8001/consumers/$CONSUMER_ID/jwt -F "key=http://172.17.0.1:8080/auth/realms/master" -F "algorithm=RS256" -F "rsa_public_key=@./rsa_pub_key.pem"

curl -X PATCH http://localhost:8001/plugins/$JWT_PLUGIN_ID  --data "config.claims_to_verify=exp,nbf"
