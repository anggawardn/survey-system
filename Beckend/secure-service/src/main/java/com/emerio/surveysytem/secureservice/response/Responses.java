package com.emerio.surveysytem.secureservice.response;


public class Responses {

    private String message;

    public Responses(String message) {
		this.message = message;
	}

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    
}