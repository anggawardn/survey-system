package com.emerio.surveysytem.secureservice.model;


public class RefreshToken {

    private String refresh_token;

    public String getRefresh_token() {
        return this.refresh_token;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }

}