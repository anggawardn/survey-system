package com.emerio.surveysytem.secureservice.controller;

import com.emerio.surveysytem.secureservice.exception.NotAuthorizedException;
import com.emerio.surveysytem.secureservice.model.RefreshToken;
import com.emerio.surveysytem.secureservice.model.User;
import com.emerio.surveysytem.secureservice.model.UserCredentials;
import com.emerio.surveysytem.secureservice.response.Responses;
import com.emerio.surveysytem.secureservice.services.KeyCloakServices;

import org.keycloak.representations.AccessTokenResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class KeycloakController {


	@Autowired
	KeyCloakServices keyClockService;

	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public AccessTokenResponse login(@RequestBody UserCredentials userCredentials) throws NotAuthorizedException {
		
		return keyClockService.login(userCredentials);
	}


	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public ResponseEntity<Responses> createUser(@RequestBody User user) {

		int responCode;
		try {

			responCode = keyClockService.createUserInKeyCloak(user);
			
			if(responCode == 201) {

				return new ResponseEntity<>(new Responses("User Registered Successfully"), HttpStatus.OK);

			}
			else if (responCode == 409) {

				return new ResponseEntity<>(new Responses("User Already Used"), HttpStatus.CONFLICT);
			}
			else {

				return new ResponseEntity<>(new Responses("User could not be created"), HttpStatus.BAD_REQUEST);
			}

		}

		catch (Exception ex) {

		ex.printStackTrace();
			return new ResponseEntity<>(new Responses("User could not be created"), HttpStatus.BAD_REQUEST);

		}

	}

	@RequestMapping(value = "/refresh", method = RequestMethod.POST)
	public ResponseEntity<?> getTokenUsingRefreshToken(@RequestBody RefreshToken refresh_token) {

		String responseToken = null;
		try {

			responseToken = keyClockService.getRefreshToken(refresh_token);

			if(responseToken == null) {

				return new ResponseEntity<>(new Responses("Refresh token can not be used"), HttpStatus.BAD_REQUEST);
			}
			else {

				return new ResponseEntity<>(responseToken, HttpStatus.OK);
			}


		} catch (Exception e) {

			e.printStackTrace();
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

	}

}
