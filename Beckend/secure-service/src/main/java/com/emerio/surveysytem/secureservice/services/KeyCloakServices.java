package com.emerio.surveysytem.secureservice.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.core.Response;

import com.emerio.surveysytem.secureservice.exception.NotAuthorizedException;
import com.emerio.surveysytem.secureservice.model.RefreshToken;
import com.emerio.surveysytem.secureservice.model.User;
import com.emerio.surveysytem.secureservice.model.UserCredentials;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.AccessTokenResponse;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Component
public class KeyCloakServices { 

    public static final Logger log = LoggerFactory.getLogger(KeyCloakServices.class);

	@Value("${keycloak.credentials.secret}")
	private String SECRETKEY;

	@Value("${keycloak.resource}")
	private String CLIENTID;

	@Value("${keycloak.auth-server-url}")
	private String AUTHURL;

	@Value("${keycloak.realm}")
	private String REALM;

	public AccessTokenResponse login(UserCredentials userCredentials) throws NotAuthorizedException {
		
		try {

		    Keycloak keycloak = KeycloakBuilder
		            .builder()
		            .serverUrl(AUTHURL)
		            .realm(REALM)
		            .username(userCredentials.getUsername())
		            .password(userCredentials.getPassword())
					.clientId(CLIENTID)
					.clientSecret(SECRETKEY)
		            .resteasyClient(new ResteasyClientBuilder().connectionPoolSize(10).build())
		            .build();
		    
			return keycloak.tokenManager().getAccessToken();
		} 

		catch (Exception ex) {
    		log.error("Unauthorized access to protected resource", ex);
    		throw new NotAuthorizedException("Unauthorized access to protected resource");
		}
	}

	public String getRefreshToken(RefreshToken refresh_token) {

		String responseToken = null;
		try {

			List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
			urlParameters.add(new BasicNameValuePair("grant_type", "refresh_token"));
			urlParameters.add(new BasicNameValuePair("client_id", CLIENTID));
			urlParameters.add(new BasicNameValuePair("refresh_token", refresh_token.getRefresh_token()));
			urlParameters.add(new BasicNameValuePair("client_secret", SECRETKEY));

			responseToken = sendPost(urlParameters);
			
		} catch (Exception e) {
			e.printStackTrace();

		}

		return responseToken;
	}


	public int createUserInKeyCloak(User user) {
		
		int statusId = 0;
		try {

			UsersResource userRessource = getKeycloakUserResource();

			UserRepresentation userRep = new UserRepresentation();
			userRep.setUsername(user.getUsername());
			userRep.setEmail(user.getEmail());
			userRep.setFirstName(user.getFirstname());
			userRep.setLastName(user.getLastname());
			userRep.setEnabled(true);

			// Create user
			Response result = userRessource.create(userRep);
			System.out.println("Keycloak create user response code>>>>" + result.getStatus());

			statusId = result.getStatus();

			if (statusId == 201) {

				String userId = result.getLocation().getPath().replaceAll(".*/([^/]+)$", "$1");

				System.out.println("User created with userId:" + userId);

				// Define password credential
				CredentialRepresentation passwordCred = new CredentialRepresentation();
				passwordCred.setTemporary(false);
				passwordCred.setType(CredentialRepresentation.PASSWORD);
				passwordCred.setValue(user.getPassword());

				// Set password credential
				userRessource.get(userId).resetPassword(passwordCred);

				// set role
				// RealmResource realmResource = getRealmResource();
				// RoleRepresentation savedRoleRepresentation = realmResource.roles().get("user").toRepresentation();
				// realmResource.users().get(userId).roles().realmLevel().add(Arrays.asList(savedRoleRepresentation));

				System.out.println("Username = " + user.getUsername() + " created in keycloak successfully");

			}

			else if (statusId == 409) {
				System.out.println("Username = " + user.getUsername() + " already present in keycloak");

			} 
			else {
				System.out.println("Username = " + user.getUsername() + " could not be created in keycloak");

			}

		} catch (Exception e) {
			e.printStackTrace();

		}

		return statusId;

	}

	private UsersResource getKeycloakUserResource() {

		Keycloak kc = KeycloakBuilder.builder().serverUrl(AUTHURL).realm("master").username("admin").password("admin")
				.clientId("admin-cli").resteasyClient(new ResteasyClientBuilder().connectionPoolSize(10).build())
				.build();

		RealmResource realmResource = kc.realm(REALM);
		UsersResource userRessource = realmResource.users();

		return userRessource;
	}

	private RealmResource getRealmResource() {

		Keycloak kc = KeycloakBuilder.builder().serverUrl(AUTHURL).realm("master").username("admin").password("admin")
				.clientId("admin-cli").resteasyClient(new ResteasyClientBuilder().connectionPoolSize(10).build())
				.build();

		RealmResource realmResource = kc.realm(REALM);

		return realmResource;

	}

	public String sendPost(List<NameValuePair> urlParameters) throws Exception {

		HttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(AUTHURL + "/realms/" + REALM + "/protocol/openid-connect/token");

		post.setEntity(new UrlEncodedFormEntity(urlParameters));

		HttpResponse response = client.execute(post);

		ResponseHandler<String> responseHandler = new BasicResponseHandler();

		String responseBody = responseHandler.handleResponse(response);
		String badRequest = "Bad Request";
		String serverError = "Internal Server Error";
		int code = response.getStatusLine().getStatusCode();
		
		if(code == 200) {

			System.out.println("Refresh Token Success: code is " + code);

			return responseBody.toString();
		}
		else if (code == 400) {

			System.out.println("Bad Request: code is " + code);

			return badRequest;
	
		}
		else {

			System.out.println("Internal Server Error: code is " + code);
			
			return serverError;
		}
		
	}

}