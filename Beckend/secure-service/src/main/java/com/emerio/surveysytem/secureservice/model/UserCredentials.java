package com.emerio.surveysytem.secureservice.model;



public class UserCredentials {

    private String password;

    private String username;
    

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
 
}