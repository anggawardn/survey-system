package com.emerio.surveysystem.repo;

import java.util.List;

import com.emerio.surveysystem.model.Survey;
import com.emerio.surveysystem.model.SurveyView;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SurveyRepo extends JpaRepository<Survey, Integer> {

    List<SurveyView> findBy(); // to find all survey using and implement the projection SurveyView
}