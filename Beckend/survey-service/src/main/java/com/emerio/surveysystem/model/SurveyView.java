package com.emerio.surveysystem.model;

import org.springframework.stereotype.Component;

@Component
public interface SurveyView {

    int getId();

    String getTitle();

    String getDesc();
}