package com.emerio.surveysystem.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
// import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "option_answer")
public class OptionAnswer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String value;
    @Column(name = "option_answer_description")
    private String desc;

    @JsonIgnore
    @JoinColumn(name = "question_id", referencedColumnName = "question_id", nullable = true)
    @ManyToOne
    // @JsonManagedReference
    private Question question;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

}