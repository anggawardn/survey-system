package com.emerio.surveysystem.repo;

import com.emerio.surveysystem.model.OptionAnswer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OptionAnswerRepo extends JpaRepository<OptionAnswer, Integer> {

}