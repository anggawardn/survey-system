package com.emerio.surveysystem.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

// import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
// import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "question")
public class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "question_id")
    private int id;
    private String question;
    private String questionType;
    private String imageUrl;
    private String videoUrl;
    private int weight;
    private String answerKey;
    @Column(name = "question_order")
    private int order;

    @JsonIgnore
    @JoinColumn(name = "page_id", referencedColumnName = "page_id", nullable = true)
    @ManyToOne // (optional = false)
    // @JsonManagedReference
    private Page page;

    @OneToMany(mappedBy = "question", cascade = CascadeType.PERSIST)
    // @JsonBackReference
    private List<OptionAnswer> optionAnswers = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getQuestionType() {
        return questionType;
    }

    public void setQuestionType(String questionType) {
        this.questionType = questionType;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getAnswerKey() {
        return answerKey;
    }

    public void setAnswerKey(String answerKey) {
        this.answerKey = answerKey;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public List<OptionAnswer> getOptionAnswers() {
        return optionAnswers;
    }

    public void addOptionAnswers(OptionAnswer optionAnswer) {
        this.optionAnswers.add(optionAnswer);
    }

    public void removeOptionAnswers(OptionAnswer optionAnswer) {
        this.optionAnswers.remove(optionAnswer);
    }

}