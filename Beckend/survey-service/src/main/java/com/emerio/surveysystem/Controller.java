package com.emerio.surveysystem;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import com.emerio.surveysystem.model.OptionAnswer;
import com.emerio.surveysystem.model.Page;
import com.emerio.surveysystem.model.Question;
import com.emerio.surveysystem.model.Survey;
import com.emerio.surveysystem.model.SurveyView;
import com.emerio.surveysystem.repo.OptionAnswerRepo;
import com.emerio.surveysystem.repo.PageRepo;
import com.emerio.surveysystem.repo.QuestionRepo;
import com.emerio.surveysystem.repo.SurveyRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api")
public class Controller {

    @Autowired
    EntityManager em;

    @Autowired
    SurveyRepo surveyRepo;

    @Autowired
    PageRepo pageRepo;

    @Autowired
    QuestionRepo questionRepo;

    @Autowired
    OptionAnswerRepo optionAnswerRepo;

    @GetMapping(value = "/show-all-survey")
    public ResponseEntity<List<SurveyView>> showAllSurveyByView() {

        try {
            return new ResponseEntity<List<SurveyView>>(surveyRepo.findBy(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping(value = "/find-all-survey")
    public ResponseEntity<List<Survey>> showAllSurvey() {

        try {
            return new ResponseEntity<List<Survey>>(surveyRepo.findAll(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping(value = "find-all-page")
    public ResponseEntity<List<Page>> showAllPage() {

        try {
            return new ResponseEntity<List<Page>>(pageRepo.findAll(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping(value = "find-all-question")
    public ResponseEntity<List<Question>> showAllQuestion() {

        try {
            return new ResponseEntity<List<Question>>(questionRepo.findAll(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping(value = "find-all-optionAnswer")
    public ResponseEntity<List<OptionAnswer>> showAllOptionAnswer() {

        try {
            return new ResponseEntity<List<OptionAnswer>>(optionAnswerRepo.findAll(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @Transactional
    @PostMapping(value = "/add-existing-page-to-survey-by-pageId-and-surveyId/{page_id}/{survey_id}")
    public ResponseEntity<String> addExistingPageToSurveyById(@PathVariable(name = "page_id") int page_id,
            @PathVariable(name = "survey_id") int survey_id) {
        try {
            Optional<Survey> survey = surveyRepo.findById(survey_id);
            Optional<Page> page = pageRepo.findById(page_id);
            if (survey.isPresent() && page.isPresent()) {
                survey.get().addPages(page.get());
                page.get().setSurvey(survey.get());
                return new ResponseEntity<String>("Added", HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @Transactional
    @PostMapping(value = "/add-page-to-survey-by-surveyId/{survey_id}")
    public ResponseEntity<Survey> addPageToSurveyById(@PathVariable(name = "survey_id") int id,
            @RequestBody Page page) {

        try {
            Optional<Survey> survey = surveyRepo.findById(id);
            if (survey.isPresent()) {
                survey.get().addPages(page);
                page.setSurvey(survey.get());
                return new ResponseEntity<Survey>(survey.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @Transactional
    @RequestMapping(value = "/add-all-at-once", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> addallAtOnce(@RequestBody Survey survey) {

        try {
            em.persist(survey); // saat di survey di persist, karena cascade tipenya persist, maka semua child
            // table nya akan otomatis di persist
            survey.getPages().forEach(p -> {
                p.setSurvey(survey); // tiap page di set surveynya
                if (!p.getQuestions().isEmpty()) { // jika ada question di tiap page
                    p.getQuestions().forEach(q -> { // untuk setiap question
                        q.setPage(p); // question di set page nya
                        if (!q.getOptionAnswers().isEmpty()) {
                            q.getOptionAnswers().forEach(op -> {
                                op.setQuestion(q);
                            });
                        }
                    });
                }
            });

            return new ResponseEntity<String>("Done", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping(value = "/show-survey-by-surveyId/{survey_id}")
    public ResponseEntity<Survey> showSurveyById(@PathVariable(name = "survey_id") int id) {

        try {
            Optional<Survey> survey = surveyRepo.findById(id);
            if (survey.isPresent()) {
                return new ResponseEntity<Survey>(survey.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping(value = "/show-pages-by-surveyId/{survey_id}")
    public ResponseEntity<List<Page>> showPagesbySurveyId(@PathVariable(name = "survey_id") int id) {

        try {
            Optional<Survey> survey = surveyRepo.findById(id);
            if (survey.isPresent()) {
                return new ResponseEntity<List<Page>>(survey.get().getPages(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping(value = "/show-questions-by-PageId/{page_id}")
    public ResponseEntity<List<Question>> showQuestionbyPageId(@PathVariable(name = "page_id") int id) {

        try {
            Optional<Page> page = pageRepo.findById(id);
            if (page.isPresent()) {
                return new ResponseEntity<List<Question>>(page.get().getQuestions(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

}