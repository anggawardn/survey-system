package com.emerio.surveysystem.repo;

import com.emerio.surveysystem.model.Page;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PageRepo extends JpaRepository<Page, Integer> {

}