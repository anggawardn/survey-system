package com.emerio.surveysystem.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

// import com.fasterxml.jackson.annotation.JsonBackReference;
// import com.fasterxml.jackson.annotation.JsonFilter;
// import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "survey")
// @JsonFilter("myFilter")
public class Survey {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "survey_id")
    private int id;
    @Column(name = "title")
    private String title;
    @Column(name = "survey_description")
    private String desc;
    private String surveyType;
    private String createdBy;
    @CreationTimestamp
    private LocalDateTime createdTime;
    private String theme;
    private Boolean pub;

    // @JsonIgnore
    @OneToMany(mappedBy = "survey", cascade = CascadeType.PERSIST)
    // @JsonBackReference //dipakai jika tidak ingin menampilkan the many side
    private List<Page> pages = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getSurveyType() {
        return surveyType;
    }

    public void setSurveyType(String surveyType) {
        this.surveyType = surveyType;
    }

    public LocalDateTime getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(LocalDateTime createdTime) {
        this.createdTime = createdTime;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public Boolean getPub() {
        return pub;
    }

    public void setPub(Boolean pub) {
        this.pub = pub;
    }

    public List<Page> getPages() {
        return pages;
    }

    public void addPages(Page page) {
        this.pages.add(page);
        page.setSurvey(this);
    }

    public void removePages(Page page) {
        this.pages.remove(page);
    }

}