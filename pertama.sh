#!/bin/sh
sudo docker-compose up -d kong-db
sudo docker-compose ps
sudo docker-compose run --rm kong kong migrations up
sudo docker-compose up -d kong
sudo docker-compose ps

sudo docker-compose up -d keycloak-db
sudo docker-compose ps
sudo docker-compose up -d keycloak
sudo docker-compose ps
